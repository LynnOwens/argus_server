package net.tofweb.argus.server.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.tofweb.argus.domain.TokenRequest;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface TokenRequestRepository extends PagingAndSortingRepository<TokenRequest, Long> {
    List<TokenRequest> findByName(@Param("name") String name);
}